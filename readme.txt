# Service 1: Validation of Unique entity number (UEN) of company

User will input an UEN. Verify the string comply with the correct format.

Refer to (https://www.uen.gov.sg/ueninternet/faces/pages/admin/aboutUEN.jspx) for the validation criteria.

Can be accessed on desktop systems with a modern web browser (e.g. Google Chrome).

## Getting Started

To run the application locally, please perform the following steps
1. Clone the repository (`git clone https://gitlab.com/pengjm18/validateuniqueentitynumbersg`).
2. Load the application by opening `index.html` file in your browser.
3. Enter a valid UEN in Singapore (e.g. T09LL0001B).