'use strict';

const appendUENStatus = (function() {
  return function(text) {
    document.getElementById('UENStatusText').textContent = text;
  };
}());

const handleUENValidation = (function() {
  const entityTypeIndicators = [
    'LP', 'LL', 'FC', 'PF', 'RF', 'MQ', 'MM', 'NB', 'CC', 'CS',
    'MB', 'FM', 'GS', 'DP', 'CP', 'NR', 'CM', 'CD', 'MD', 'HS',
    'VH', 'CH', 'MH', 'CL', 'XL', 'CX', 'HC', 'RP', 'TU', 'TC',
    'FB', 'FN', 'PA', 'PB', 'SS', 'MC', 'SM', 'GA', 'GB'
  ];
  return function(searchUEN) {
    appendUENStatus('');
    let validateResult = true;
    if (searchUEN.length === 0 || searchUEN.length < 9 ||
        searchUEN.length > 10) {
      validateResult = false;
    }
    const root = searchUEN.toUpperCase().split('');
    if (root.length === 9) {
      root.forEach(function(value, key) {
        if (key < root.length - 1 && !Number.isInteger(parseInt(value))) {
          validateResult = false;
        } else if (
            key === root.length - 1 && Number.isInteger(parseInt(value))) {
          validateResult = false;
        }
      });
    } else if (root.length === 10) {
      let validateUENType = false;
      let entityPQType = '';
      root.forEach(function(value, key) {
        if (key === 0 && (value === 'T' || value === 'S' || value === 'R')) {
          validateUENType = true;
        }
        if (validateUENType) {
          if ((((key === 1 || key === 2) ||
                (key < root.length - 1 && key > 4)) &&
               !Number.isInteger(parseInt(value))) ||
              (key === 3 && Number.isInteger(parseInt(value)))) {
            validateResult = false;
            validateUENType = false;
          } else {
            const matchingEntityPQType = entityTypeIndicators.find(
                entityTypeIndicator => entityTypeIndicator === entityPQType);
            if (matchingEntityPQType) {
              validateResult = true;
            } else {
              validateResult = false;
            }
          }
          if ((key === 3 || key === 4)) {
            entityPQType += value;
          }
        } else {
          if (key < root.length - 1 && !Number.isInteger(parseInt(value))) {
            validateResult = false;
          }
        }
        if (key === root.length - 1 && Number.isInteger(parseInt(value))) {
          validateResult = false;
        }
      });
    }
    if (validateResult) {
      appendUENStatus('Thank you. UEN entered is in correct format.');
    } else {
      appendUENStatus(
          'Sorry. UEN entered is in incorrect format. Please try again.');
    }
  }
}());

(function initSubmitButton() {
  document.getElementById('submitButton')
      .addEventListener('click', function handleSubmitButtonClick(event) {
        event.preventDefault();
        handleUENValidation(document.getElementById('UENText').value);
      }, false);
}());